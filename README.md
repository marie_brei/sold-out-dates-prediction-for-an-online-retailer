# Prediction of sold-out dates for an online retailer 

This repository represents a solution to an international data mining challenge for students organized by prudsys AG in 2018.
The task was to predict sold-out dates for products of an online sport retailer for February 2018 based on sales data from the beginning of Ocotber 2017 until the end of January 2018.
A more detailed description can be found in the pdf files. 

The presented solution reached place 14 out of 193 registered teams and 63 solutions that were handed in. 
